# Drush Lint

Tools for linting drupal files with drush.

    drush lint (php | js | css)


## Dependencies:
*  PHP > 5.3
*  (Optional) Node.js
*  (Optional) Rhino (Mozilla JS Java runner)
   *  Ubuntu: `[sudo] apt-get install rhino`
   *  OSX ([homebrew](http://mxcl.github.com/homebrew/)): `[sudo] brew install rhino`

## Lint tools:
*  PHP
*  JS
*  CSS
