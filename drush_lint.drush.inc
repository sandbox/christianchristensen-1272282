<?php
/**
 * @file
 *  Drush commands to help lint files in Drupal.
 */

/**
 * Implementation of hook_drush_command().
 */
function drush_lint_drush_command() {
  $items = array();

  $items['drush-lint'] = array(
    'description' => 'Lint files in drupal',
    'arguments' => array(
      'type' => 'Lint specific file type (php, js, css)',
    ),
    'options' => array(
      'files="/list/of/files.php, /list/of/files.js"' => 'Comma separated list of (absolute pathed) files to lint',
      'path=/path/to/lint/' => 'Absolute base path to find files to lint',
      'xml=/output/directory' => 'Directory for report generation output',
      'blacklist=filename' => 'Blacklist specific files from linting',
      'hudson' => 'Does not output error info - uses warnings instead.'
    ),
    'aliases' => array('lint'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implementaton of hook_drush_help().
 */
function drush_lint_drush_help($section) {
  switch ($section) {
    case 'drush:lint':
      return dt('Lint files in Drupal codebase');
  }
}

/**
 * Implementation of drush_COMMAND() for lint
 */
function drush_drush_lint($type = NULL) {
  drush_log(dt("Lint type: " . $type), 'notice');
  // If we are not provided a file list go and search our drupal directory for assets
  if (!drush_get_option('files')) {
    if (!drush_get_option('path')) {
      $path_str = "`pwd`/";
    }
    else {
      $path_str = drush_get_option('path');
    }
    $files = array();
    $types = array("php", "js", "css");
    if (!is_null($type)){
      $types = array_intersect($types, array($type));
    }
    drush_log(dt("Lint types: " . implode(', ', $types)), 'notice');
    foreach ($types as $iter_type) {
      switch($iter_type) {
        case "php":
          $file_regex = get_extension_find_iregex(php_get_extensions());
          drush_log(dt("Parsing files with: " . $file_regex), 'notice');
          exec("find {$path_str} | grep \"{$file_regex}\" 2> /dev/null", $phpfiles);
          $files = array_merge($files, $phpfiles);
        break;
        case "js":
          $file_regex = get_extension_find_iregex(js_get_extensions());
          drush_log(dt("Parsing files with: " . $file_regex), 'notice');
          exec("find {$path_str} | grep \"{$file_regex}\" 2> /dev/null", $jsfiles);
          $files = array_merge($files, $jsfiles);
        break;
        case "css":
          $file_regex = get_extension_find_iregex(css_get_extensions());
          drush_log(dt("Parsing files with: " . $file_regex), 'notice');
          exec("find {$path_str} | grep \"{$file_regex}\" 2> /dev/null", $cssfiles);
          $files = array_merge($files, $cssfiles);
        break;
      }
    }
  }
  else {
    $files = drush_get_option('files');
    // Detect commas or newline, then split accordingly
    $files = explode(',', $files);
    array_walk($files, function(&$str){$str = ltrim($str);}); // >5.3 PHP
  }

  // Filter blacklisted items
  // TODO: this feels ugly
  if (drush_get_option('blacklist')) {
    $files = array_filter($files, function($file) {
      $blacklist = explode(',', drush_get_option('blacklist'));
      array_walk($blacklist, function(&$str){$str = ltrim($str);});
      foreach ($blacklist as $str) {
        if (strstr($file, $str)) {
          return FALSE;
        }
      }
      return TRUE;
    });
  }

  (is_null($type) || $type == "php") ? _drush_lint_validate_php_syntax($files) : NULL;
  (is_null($type) || $type == "js")  ? _drush_lint_validate_js_syntax($files) : NULL;
  (is_null($type) || $type == "css") ? _drush_lint_validate_css_syntax($files) : NULL;
}

function _drush_lint_validate_php_syntax($files) {
  $test = 0;
  $regex = php_get_extensions(TRUE);

  foreach ($files as $file) {
    if (!preg_match($regex, $file)) {
        // Only check PHP files.
        continue;
    }
    drush_log(dt("PHP syntax check: '!file'", array('!file' => $file)), 'notice');

    // Now to check PHP sytax.
    $lint_output = array();
    exec("php -l " . escapeshellarg($file), $lint_output, $test);
    if ($test == 0) {
        continue;
    }
    drush_get_option('hudson') ? drush_log(dt("Output: " . implode("\n", $lint_output)), 'warning') : drush_set_error('DRUSH_LINT_PHP_INVALID', implode("\n", $lint_output));
  }
}

function _drush_lint_validate_js_syntax($files) {
  $test = 0;
  $regex = js_get_extensions(TRUE);

  foreach ($files as $file) {
    if (!preg_match($regex, $file)) {
        // Only check PHP files.
        continue;
    }
    if (strstr($file, "sites/default/files")) {
      // Ignore default/files
      continue;
    }
    if (strstr($file, ".min.")) {
      // Try to ignore minimized files
      continue;
    }
    drush_log(dt("JS syntax check: '!file'", array('!file' => $file)), 'notice');

    // Now to check PHP sytax.
    $lint_output = array();
    // XML output (for junit results)
    if (drush_get_option('xml')) {
      exec(dirname(__FILE__) . "/lib/jslint-utils/scripts/jslint-to-xml.sh " . escapeshellarg($file) . " " . drush_get_option('xml'), $lint_output, $test);      
    }
    else {
      exec(dirname(__FILE__) . "/lib/jslint-utils/scripts/run-jslint.sh " . escapeshellarg($file), $lint_output, $test);      
    }
    if ($test == 0 && count($lint_output) == 1) {
        continue;
    }
    drush_get_option('hudson') ? drush_log(dt("Output: " . drush_get_option('xml') ? $file : implode("\n", $lint_output)), 'warning') : drush_set_error('DRUSH_LINT_JS_INVALID', implode("\n", $lint_output));
  }
}

function _drush_lint_validate_css_syntax($files) {
  $test = 0;
  $regex = css_get_extensions(TRUE);

  foreach ($files as $file) {
    if (!preg_match($regex, $file)) {
        // Only check PHP files.
        continue;
    }
    if (strstr($file, "sites/default/files")) {
      // Ignore default/files
      continue;
    }
    if (strstr($file, ".min.")) {
      // Try to ignore minimized files
      continue;
    }
    drush_log(dt("CSS syntax check: '!file'", array('!file' => $file)), 'notice');

    // Now to check PHP sytax.
    $lint_output = array();
    // XML output (for junit results)
    if (drush_get_option('xml')) {
      exec(dirname(__FILE__) . "/lib/jslint-utils/scripts/jslint-to-xml.sh " . escapeshellarg($file) . " " . drush_get_option('xml'), $lint_output, $test);      
    }
    else {
      exec(dirname(__FILE__) . "/lib/jslint-utils/scripts/run-jslint.sh " . escapeshellarg($file), $lint_output, $test);
    }
    if ($test == 0 && count($lint_output) == 1) {
        continue;
    }
    drush_get_option('hudson') ? drush_log(dt("Output: " . drush_get_option('xml') ? $file : implode("\n", $lint_output)), 'warning') : drush_set_error('DRUSH_LINT_CSS_INVALID', implode("\n", $lint_output));
  }
}

/**
 * Helper function to identify all PHP file extensions.
 *
 * @param $regex
 *  (default: FALSE) Arrange file extensions as a regular expression.
 * @param $reset
 *  (default: FALSE) Recalculate all extensions.
 *
 * @return
 *  Array of file extensions.
 */
if (!function_exists("php_get_extensions")) {
  function php_get_extensions($regex = FALSE, $reset = FALSE) {
    static $ext;
    if (empty($ext) || $reset) {
      $ext['raw'] = array(
        'php' => 'php',
        'phps' => 'phps',
        'module' => 'module',
        'inc' => 'inc',
        'test' => 'test',
        'install' => 'install',
        'engine' => 'engine',
        'theme' => 'theme',
      );
      $ext['regex'] = '/\.(' . implode('|', $ext['raw']) . ')$/S';
    }
    return $regex ? $ext['regex'] : $ext['raw'];
  }
}

function js_get_extensions($regex = FALSE, $reset = FALSE) {
  static $ext;
  if (empty($ext) || $reset) {
    $ext['raw'] = array(
      'js' => 'js',
    );
    $ext['regex'] = '/\.(' . implode('|', $ext['raw']) . ')$/S';
  }
  return $regex ? $ext['regex'] : $ext['raw'];
}

function css_get_extensions($regex = FALSE, $reset = FALSE) {
  static $ext;
  if (empty($ext) || $reset) {
    $ext['raw'] = array(
      'css' => 'css',
    );
    $ext['regex'] = '/\.(' . implode('|', $ext['raw']) . ')$/S';
  }
  return $regex ? $ext['regex'] : $ext['raw'];
}

function get_extension_find_iregex($files) {
  return '.*\.\(' . implode('\|', $files) . '\)';
}
